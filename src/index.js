import 'dotenv/config';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
 
const app = express();

app.use(bodyParser.json());
app.use(cors());

//Set Routes
var indexRouter = require('./routes/index');
var authRouter = require('./routes/admin/auth');
var postsRouter = require('./routes/post');
 
app.listen(process.env.PORT, () =>
  console.log(`Portfolio Blog is listening on port ${process.env.PORT}`),
);

app.use('/', indexRouter);
app.use('/auth', authRouter);
app.use('/posts', postsRouter);