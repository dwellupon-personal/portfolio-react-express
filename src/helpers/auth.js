const jwt = require("jsonwebtoken");
var encr = require("../helpers/encrypt");
const db = require('../models');
const Op = db.Sequelize.Op;
const User = db.User;

const jwtKey = process.env.JWT_SECRET
const jwtExpirySeconds = 2700

exports.login = function(req,res){
    new Promise(function (resolve, reject) {
        let condition = req.body.username ? { username: { [Op.eq]: req.body.username } } : null;
		
		User.findAll({ where: condition })
			.then(data => {
				if (data.length >= 1) {
					let decryptedPassword = encr.decrypt(data[0]["password"]);
					const { username, password } = req.body;

					if (!username || !password || decryptedPassword !== password) {
						// return 401 error is username or password doesn't exist, or if password does
						// not match the password in our records
						return res.status(401).json({ error: 'There username or password do not match our records' }).end()
					}
					// Create a new token with the username in the payload
					// and which expires 300 seconds after issue
					const token = jwt.sign({ username }, jwtKey, {
						algorithm: "HS256",
						expiresIn: jwtExpirySeconds,
					})
			
					// set the cookie as the token string, with a similar max age as the token
					// here, the max age is in milliseconds, so we multiply by 1000
					res.cookie("access_token", token)
					res.end()
				} else {
					return res.status(401).json({ error: 'The User does not exist in LVL0-HCI' }).end()
				}
			})
    }).catch(function (err) {
        console.log(err)
    })
    .catch(function (err) {
      const message =
        "Invalid username or password: please check your credentials" +
        " and try again.";

      return res.status(401).json({ message }).end();
    });
};
