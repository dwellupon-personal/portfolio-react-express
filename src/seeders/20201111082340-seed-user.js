'use strict';
var encr = require('../helpers/encrypt')

module.exports = {
  up: queryInterface =>
    queryInterface.bulkInsert('Users', [
      {
        username: 'dwellupon',
        password: encr.encrypt('MakeThisThingSecure'),
        email: 'arttheft.situation@gmail.com',
        createdAt: new Date(),
        updatedAt: new Date(),
        deletedAt: null
      }
    ], {}),
  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Users', null, {});
  }
};
