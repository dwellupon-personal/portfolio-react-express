const Post = require('../models').Post;

exports.findAll = (req, res) => {
    Post.findAll()
    .then(data => {
        res.json({
            "message":"success",
            "data": data
        });
    })
    .catch(err => {
        res.status(500).send({
            message:
            err.message || "An error occurred while retrieving all posts."
        });
    });
};

exports.findOne = (req, res) => {
    const slug = req.params.postSlug;

    Post.findOne({
        where: {
            post_slug: slug
            }
        }
    )
    .then(data => {
        res.json({
            "message":"success",
            "data": data
        });
    })
    .catch(err => {
        res.status(500).send({
            message:
            err.message || "An error occurred while retrieving all posts."
        });
    });
};

exports.create = (req, res) => {
    if (!req.body.post_name) {
        res.status(400).send({
            message: "New posts must have a post name."
        });
        return;
    }

    if (!req.body.post_content) {
        res.status(400).send({
            message: "New posts must have post content."
        });
        return;
    }

    let slugLc = req.body.post_name.toLowerCase(),
        slug   = slugLc.split(' ').join('_');
        
    const post = {
        post_name:req.body.post_name,
        post_slug:slug,
        post_content:req.body.post_content,
        published:req.body.published,
    }

    Post.create(post)
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "An error occurred creating the post."
        });
    });
};