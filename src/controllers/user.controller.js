const db = require('../models');
const User = db.User;

exports.findAll = (req, res) => {
    User.findAll()
        .then(data => {
            res.json({
                "message":"success",
                "data": data
            });
        })
        .catch(err => {
            res.status(500).send({
              message:
                err.message || "An error occurred while retrieving all users."
            });
        });
};