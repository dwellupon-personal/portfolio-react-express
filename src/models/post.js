'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Post extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Post.init({
    post_name: DataTypes.STRING,
    post_slug: DataTypes.STRING,
    post_content: DataTypes.TEXT,
    published:DataTypes.BOOLEAN

  }, {
    sequelize,
    modelName: 'Post',
  });
  return Post;
};