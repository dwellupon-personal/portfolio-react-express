var express = require('express');
var router = express.Router(); 

const users = require('../controllers/user.controller.js');

router.get("/:userid", users.findOne);

module.exports = router;