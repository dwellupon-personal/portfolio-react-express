var express = require('express');
var router = express.Router(); 

const posts = require('../controllers/posts.controller');

router.get("/", posts.findAll)
      .post("/", posts.create);

router.get("/:postSlug", posts.findOne)

module.exports = router;