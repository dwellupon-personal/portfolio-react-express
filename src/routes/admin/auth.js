var express = require('express');
var router = express.Router(); 
const auth = require("../../helpers/auth");

router.post('/', function(req, res, next) {
   if (req.body.username != undefined && req.body.password != undefined) {
      const authStat = auth.login(req,res);
   } else {
      res.status(401).json({ error: 'User Credentials are required' }).end()
   }
});

module.exports = router;