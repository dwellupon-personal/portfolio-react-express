require("dotenv").config();
var express = require('express');
var router = express.Router();

/* Root returns application version */
router.get('/', function(req, res, next) {
    res.send(process.env.APP_VERSION);
});

module.exports = router;